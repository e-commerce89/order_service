CREATE TYPE "payment_methods" AS ENUM ('UZCARD', 'VISA', 'CASH');
CREATE TYPE "payment_status" AS ENUM ('PAID', 'UNPAID');
CREATE TYPE "order_status" AS ENUM ('NOTSTARTED', 'INPROCESS','DELIVERED');

CREATE TABLE IF NOT EXISTS "order" (
	"id" CHAR(36) PRIMARY KEY,
	"user_id" CHAR(36)  NOT NULL,
	"customer_name" VARCHAR(255) NOT NULL,
	"customer_address" VARCHAR(255) NOT NULL ,
	"customer_phone"   VARCHAR NOT NULL,
	"payment_method" payment_methods NOT NULL,
	"payment_status" payment_status NOT NULL,
	"status" order_status NOT NULL,
	"total_price" INT,
	"created_at" TIMESTAMP DEFAULT now() NOT NULL
);

CREATE TABLE IF NOT EXISTS "order_items" (
   	"id" CHAR(36) PRIMARY KEY,
   	"order_id" CHAR(36)  REFERENCES "order"("id")NOT NULL,
   	"product_id" CHAR(36) NOT NULL,
	"quantity" INT NOT NULL
	
);

CREATE TABLE IF NOT EXISTS "shopping_cart_items" (
   	"id" CHAR(36) PRIMARY KEY,
   	"user_id" CHAR(36)  NOT NULL,
   	"product_id" CHAR(36) NOT NULL,
	"quantity" INT NOT NULL
);