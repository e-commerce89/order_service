DROP TABLE IF EXISTS "shopping_cart_items";
DROP TABLE IF EXISTS "order_items";
DROP TABLE IF EXISTS "order";
DROP TYPE IF EXISTS "order_status";
DROP TYPE IF EXISTS "payment_status";
DROP TYPE IF EXISTS "payment_methods";

