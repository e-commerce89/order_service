module github.com/AbdulahadAbduqahhorov/E-commerce/order_service

go 1.19

require (
	github.com/golang/protobuf v1.5.2
	github.com/google/uuid v1.3.0
	github.com/jmoiron/sqlx v1.3.5
	github.com/joho/godotenv v1.4.0
	github.com/lib/pq v1.10.7
	github.com/spf13/cast v1.5.0
	github.com/streamingfast/logging v0.0.0-20221209193439-bff11742bf4c
	go.uber.org/zap v1.24.0
	golang.org/x/crypto v0.5.0
	google.golang.org/grpc v1.52.1
	google.golang.org/protobuf v1.28.1
)

require (
	github.com/blendle/zapdriver v1.3.1 // indirect
	github.com/logrusorgru/aurora v2.0.3+incompatible // indirect
	github.com/mitchellh/go-testing-interface v1.14.1 // indirect
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
	golang.org/x/net v0.5.0 // indirect
	golang.org/x/sys v0.4.0 // indirect
	golang.org/x/term v0.4.0 // indirect
	golang.org/x/text v0.6.0 // indirect
	google.golang.org/genproto v0.0.0-20221118155620-16455021b5e6 // indirect
)
