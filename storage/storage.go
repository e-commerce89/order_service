package storage

import (
	"github.com/AbdulahadAbduqahhorov/E-commerce/order_service/storage/postgres"
	"github.com/AbdulahadAbduqahhorov/E-commerce/order_service/storage/repo"
	"github.com/jmoiron/sqlx"
)

type StorageI interface {
	Order() repo.OrderRepoI
	Cart() repo.CartRepoI
}

type storagePg struct {
	db    *sqlx.DB
	order repo.OrderRepoI
	cart  repo.CartRepoI
}

func NewStoragePg(db *sqlx.DB) StorageI {
	return &storagePg{
		db:    db,
		order: postgres.NewOrderRepo(db),
		cart:  postgres.NewCartRepo(db),
	}
}

func (s *storagePg) Order() repo.OrderRepoI {
	return s.order
}
func (s *storagePg) Cart() repo.CartRepoI {
	return s.cart
}
