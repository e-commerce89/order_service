package repo

import "github.com/AbdulahadAbduqahhorov/E-commerce/order_service/genproto/order_service"

type OrderRepoI interface {
	CreateOrder(id string, total int, req *order_service.CreateOrderRequest) (*order_service.CreateOrderResponse, error)
	GetOrderList(req *order_service.GetOrderListRequest) (*order_service.GetOrderListResponse, error)
	GetOrderById(req *order_service.GetOrderByIdRequest) (*order_service.OrderInfo, error)
	UpdateOrder(req *order_service.UpdateOrderRequest) (rowsAffected int64, err error)
	DeleteOrder(id string) (rowsAffected int64, err error)
}
