package repo

import cart_service "github.com/AbdulahadAbduqahhorov/E-commerce/order_service/genproto/order_service"

type CartRepoI interface {
	CreateCart(id string, req *cart_service.CreateCartRequest) (error)
	GetCartList(req *cart_service.GetCartListRequest) (*cart_service.GetCartListResponse, error)
	GetCartById(req *cart_service.GetCartByIdRequest) (*cart_service.GetCartByIdResponse, error)
	UpdateCart(req *cart_service.UpdateCartRequest) (rowsAffected int64, err error)
	DeleteCart(req *cart_service.DeleteCartRequest) (rowsAffected int64, err error)
}
