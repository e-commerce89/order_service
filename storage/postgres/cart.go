package postgres

import (
	cart_service "github.com/AbdulahadAbduqahhorov/E-commerce/order_service/genproto/order_service"

	"github.com/AbdulahadAbduqahhorov/E-commerce/order_service/storage/repo"
	"github.com/jmoiron/sqlx"
)

type cartRepo struct {
	db *sqlx.DB
}

func NewCartRepo(db *sqlx.DB) repo.CartRepoI {
	return &cartRepo{db: db}
}

func (c *cartRepo) CreateCart(id string, req *cart_service.CreateCartRequest) error {
	_, err := c.db.Exec(`INSERT INTO "shopping_cart_items" 
	(
		id,
		user_id,
		product_id,
		quantity
	) VALUES (
		$1,
		$2,
		$3,
		$4
	)`,
		id,
		req.UserId,
		req.ProductId,
		req.Quantity,
	)
	if err != nil {
		return err
	}

	return nil
}

func (c *cartRepo) GetCartById(req *cart_service.GetCartByIdRequest) (*cart_service.GetCartByIdResponse, error) {
	res := &cart_service.GetCartByIdResponse{
		CartItem: &cart_service.CartItem{},
	}

	err := c.db.QueryRow(`SELECT 
		id,
		user_id,
		product_id,
		quantity
    FROM "shopping_cart_items" WHERE product_id = $1 AND user_id=$2`, req.ProductId, req.UserId).Scan(
		&res.CartItem.Id,
		&res.CartItem.UserId,
		&res.CartItem.ProductId,
		&res.CartItem.Quantity,
	)
	if err != nil {
		return res, err
	}
	return res, err
}

// GetUserList ...
func (c *cartRepo) GetCartList(req *cart_service.GetCartListRequest) (*cart_service.GetCartListResponse, error) {
	resp := &cart_service.GetCartListResponse{
		Carts: make([]*cart_service.CartItem, 0),
	}

	rows, err := c.db.Queryx(`SELECT
	id,
	user_id,
	product_id,
	quantity
	FROM "shopping_cart_items" WHERE user_id=$1
	LIMIT $2
	OFFSET $3
	`, req.UserId, req.Limit, req.Offset)
	if err != nil {
		return resp, err
	}

	for rows.Next() {
		a := &cart_service.CartItem{}

		err := rows.Scan(
			&a.Id,
			&a.UserId,
			&a.ProductId,
			&a.Quantity,
		)
		if err != nil {
			return resp, err
		}

		resp.Carts = append(resp.Carts, a)
	}

	return resp, err
}

// UpdateUser ...
func (c *cartRepo) UpdateCart(req *cart_service.UpdateCartRequest) (rowsAffected int64, err error) {
	res, err := c.db.NamedExec(`UPDATE "shopping_cart_items" SET quantity=:q  WHERE product_id=:p_id AND user_id=:u_id`, map[string]interface{}{
		"q":    req.Quantity,
		"p_id":   req.ProductId,
		"u_id": req.UserId,
	})
	if err != nil {
		return 0, err
	}

	n, err := res.RowsAffected()
	if err != nil {
		return 0, err
	}

	return n, nil
}

// DeleteUser ...
func (c *cartRepo) DeleteCart(req *cart_service.DeleteCartRequest) (rowsAffected int64, err error) {
	query := `DELETE FROM "shopping_cart_items" WHERE product_id = $1 AND user_id=$2`
	res, err := c.db.Exec(query, req.ProductId, req.UserId)
	if err != nil {
		return 0, err
	}

	n, err := res.RowsAffected()
	if err != nil {
		return 0, err
	}

	return n, nil
}
