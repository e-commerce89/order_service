package service

import (
	"context"

	"github.com/AbdulahadAbduqahhorov/E-commerce/order_service/client"
	cart_service "github.com/AbdulahadAbduqahhorov/E-commerce/order_service/genproto/order_service"

	// "github.com/AbdulahadAbduqahhorov/E-commerce/order_service/genproto/product_service"
	"github.com/AbdulahadAbduqahhorov/E-commerce/order_service/pkg/logger"
	"github.com/AbdulahadAbduqahhorov/E-commerce/order_service/storage"
	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type cartService struct {
	log      logger.LoggerI
	storage  storage.StorageI
	services client.ServiceManagerI
	cart_service.UnimplementedCartServiceServer
}

func NewCartService(log logger.LoggerI, db *sqlx.DB, srvc client.ServiceManagerI) *cartService {
	return &cartService{
		log:      log,
		storage:  storage.NewStoragePg(db),
		services: srvc,
	}
}

func (s *cartService) Create(ctx context.Context, req *cart_service.CreateCartRequest) (*cart_service.CreateCartResponse, error) {
	s.log.Info("---CreateCart--->", logger.Any("req", req))
	id := uuid.New().String()
	res, err := s.storage.Cart().GetCartById(&cart_service.GetCartByIdRequest{
		ProductId: req.ProductId,
		UserId:    req.UserId,
	})
	if err != nil {
		err = s.storage.Cart().CreateCart(id, req)
		if err != nil {
			s.log.Error("!!!CreateCart--->", logger.Error(err))
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}
	
		return &cart_service.CreateCartResponse{Id: id}, nil	
	}
	
	_, err = s.storage.Cart().UpdateCart(&cart_service.UpdateCartRequest{
		ProductId: res.CartItem.ProductId,
		UserId:    res.CartItem.UserId,
		Quantity:  res.CartItem.Quantity + req.Quantity,
	})
	if err != nil {
		if err != nil {
			s.log.Error("!!!CreateCart--->", logger.Error(err))
			return nil, status.Error(codes.Internal, err.Error())
		}
	}
	return &cart_service.CreateCartResponse{Id: res.CartItem.Id},nil
}

func (s *cartService) GetList(ctx context.Context, req *cart_service.GetCartListRequest) (*cart_service.GetCartListResponse, error) {
	s.log.Info("---GetCartList--->", logger.Any("req", req))
	res, err := s.storage.Cart().GetCartList(req)
	if err != nil {
		s.log.Error("!!!GetCartList--->", logger.Error(err))
		return res, status.Error(codes.Internal, err.Error())
	}

	return res, nil
}

func (s *cartService) GetById(ctx context.Context, req *cart_service.GetCartByIdRequest) (*cart_service.GetCartByIdResponse, error) {

	s.log.Info("---GetCartById--->", logger.Any("req", req))
	res, err := s.storage.Cart().GetCartById(req)
	if err != nil {
		s.log.Error("!!!GetCartById--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}
	return res, nil
}
func (s *cartService) UpdateCart(ctx context.Context, req *cart_service.UpdateCartRequest) (*emptypb.Empty, error) {
	s.log.Info("---UpdateCart--->", logger.Any("req", req))

	rowsAffected, err := s.storage.Cart().UpdateCart(req)

	if err != nil {
		s.log.Error("!!!UpdateCart--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}
	return &emptypb.Empty{}, err
}
func (s *cartService) Delete(ctx context.Context, req *cart_service.DeleteCartRequest) (*emptypb.Empty, error) {
	s.log.Info("---DeleteCart--->", logger.Any("req", req))

	rowsAffected, err := s.storage.Cart().DeleteCart(req)

	if err != nil {
		s.log.Error("!!!DeleteCart--->", logger.Error(err))
		return nil, status.Error(codes.Internal, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	return &emptypb.Empty{}, nil
}
