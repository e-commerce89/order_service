package client

import (
	"github.com/AbdulahadAbduqahhorov/E-commerce/order_service/config"
	"github.com/AbdulahadAbduqahhorov/E-commerce/order_service/genproto/catalog_service"
	"google.golang.org/grpc"
)

type ServiceManagerI interface {
	ProductService() catalog_service.ProductServiceClient
}

type grpcClients struct {
	productService catalog_service.ProductServiceClient
}

func NewGrpcClients(cfg config.Config) (ServiceManagerI, error) {
	connCatalogService, err := grpc.Dial(
		cfg.CatalogServiceHost+cfg.CatalogServicePort,
		grpc.WithInsecure(),
	)
	if err != nil {
		return nil, err
	}

	return &grpcClients{
		productService: catalog_service.NewProductServiceClient(connCatalogService),
	}, nil
}

func (g *grpcClients) ProductService() catalog_service.ProductServiceClient {
	return g.productService
}
